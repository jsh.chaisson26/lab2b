import java.util.Scanner;
import java.util.Random;
public class wordle{
	public static void main(String[] args){ //This function is used so that we can call the function ncessary to run through the whole game of wordle.
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to everyone's favourite game...\nWORDLE");
		runGame(generateWord());
	}
	public static String generateWord() { //This function creates an array of strings that are all 5 letters with no duplicate letters and creates a random element so that a random one of these 20 string will be chosen and returned.
		String[] wordleWords = new String[] {"TRUCK", "TRASH", "PLATE", "MOUSE", "AWAKE", "SPRAY", "WITCH", "WHIRL", "ADULT", "SCRUB", "SQUAT", "SHAKY", "VOCAL", "CHASE", "EXTRA", "URBAN", "HASTE", "WHALE", "IMAGE", "FLAKE"};
		Random random = new Random();
		int randomIndex = random.nextInt(wordleWords.length);
		return wordleWords[randomIndex];
	}
	public static boolean letterInWord(String word, char letter){ //This function uses a loop to check if a letter if a letter at a certain index is at any index at all or in simpler terms if a letter is in the word at all. So if the letter is in the word at all then it will return true.
		for(int i=0; i<word.length(); i++){
			if(word.charAt(i)==letter){
				return true;
			}
		}
		return false;
	}
	public static boolean letterInSlot (String word, char letter, int position) { //This function is similar to letterinWord but it checks if the letter is in the exact position in the word. It will only return true in this case.
			return word.charAt(position)==letter;  
	}
	public static String[] guessWord(String answer, String guess){ //This function creates and returns a string array that has the colour of the letters. If the colour is green, then the letter's index in the word that corresponds with the index in the colours array is in the exact right position(letterInSLot). If the colour is yellow, then the letter's index in the word that corresponds with the index in the colours array is in the word but not right position(letterInWord). It returns white in the colour array if the letter is not in the word at all. 
		String[] colours = new String[] {"white", "white", "white", "white", "white"};
		for(int i=0; i < answer.length(); i++){
			if(letterInWord(answer, guess.charAt(i))==true){
				if(letterInSlot(answer, guess.charAt(i), i)==true){
					colours[i] = "green";
				}
				else{	
					colours[i] = "yellow";
				}
			}
		}
		return colours;
	}
	public static void presentResults(String guess, String[] colours){//This function returns white around a letter if it's not in the word at all. Yellow if it's in the word but in the wrong position. Green if it's in the right position. It follows the logic of the previous function. It just adds the physical colours around the letters.
		final String ANSI_RESET = "\u001B[0m";
		final String ANSI_GREEN = "\u001B[32m";
		final String ANSI_YELLOW = "\u001B[33m";
		String toPrint = "";
		for(int i= 0; i < guess.length(); i++){
			if(colours[i].equals("green")){
				toPrint+= ANSI_GREEN + guess.charAt(i) + ANSI_RESET;
			}
			if(colours[i].equals("yellow")){
				toPrint+= ANSI_YELLOW + guess.charAt(i) + ANSI_RESET;
			}
		    if(colours[i].equals("white")){
				toPrint+= ANSI_RESET + guess.charAt(i) + ANSI_RESET;
		    }
		}
		System.out.println(toPrint);
	} 
	public static String readGuess(){//This function asks the user to guess a word. If it is not a 5 letter word it will ask them to guess again with a 5 letter word until they do it. It also returns the word they guessed uppercased.
        System.out.println();
        Scanner reader = new Scanner(System.in);
        System.out.println("Please guess a word:");
        String userChosenWord = reader.nextLine();
        while(userChosenWord.length() !=5){
            System.out.println("The word you guessed was not 5 letters.\nLearn to count and choose a 5 letter word dork:");
            userChosenWord = reader.nextLine();
        }
        userChosenWord = userChosenWord.toUpperCase();
        return userChosenWord;
    }
	public static void runGame(String word){//This function calls makes the user guess a 5 letter word. Until they either win the game or run out of guesses. Everytime they guess the will be showed which letters are green(corrct position), yellow(right letter, wrong position) and white(letter not in word) and they will be told how many lives they have left. If they win they will get a congratualatory message and be told how many tries it took them. If they lose they will get a message telling them that they lost and what the word they were trying to guess was as well as making them replay the game with a new random word.
		int lives = 6;
		boolean win = false;
		while(lives > 0 && win == false){
			String guessCheck = readGuess();
			presentResults(guessCheck, guessWord(word, guessCheck));
			lives--;
			if(word.equals(guessCheck)){
				win = true;
			}
			if(lives > 1 && win ==false){
				System.out.println("You have " + lives + " lives left.");
			}
			if(lives ==1 && win ==false){
				System.out.println("You have " + lives + " life left.");
			}
		}
		if(win){
			   System.out.println("Congratulations!\nYOU WIN!!!!!\nIt only took you " + (6 - lives) + " tries to guess the word!\nThank you for playing!");
		}
		else{
			System.out.println("The word was " + word);
			System.out.println("Eishhhh, I'm so sorry to tell you but...\nYOU LOST!\nTRY AGAIN");
			runGame(generateWord());
		}
	}
}