import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	for (int i =0; i<4 ; i++){
	//Checks if the leter is in any spots on the word.
	//If it is it returns the position of the letter on the word. If not, then it returns -1.
		if (word.charAt(i)==c){
			return i;
		}
			
	}
	   return -1;
}
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
		//Turns characters to uppercase letters so that it
		//doesn't matter whether or not uppercases are used.
	}
	public static void printWork(String word, boolean[] letters ) {
		//Checks if letter is true and if it is i puts that letter in the new word that is being guessed
		//If wrong if it puts an underscore in the spot to signify that it's missing.
		String newWord = "";
		for(int i= 0; i < letters.length; i ++){
			if(letters[i]){
				newWord+= word.charAt(i);
			}
			else{
				newWord+= " _";
			}
		}
		System.out.println("Your result is "+ newWord);
	}
	public static  void runGame(String word){
		boolean[] letters = {false, false, false, false}; 
		int lives = 6;                                   
		boolean correct = false;                         
		while(lives > 0 && correct == false){             
			Scanner reader = new Scanner(System.in);
			if(lives ==6 && letters[0] == false && letters[1] == false && letters[2] == false && letters[3] == false){
				System.out.println("Guess a letter");
			}
			char guessedLetter = reader.nextLine().charAt(0);
			guessedLetter =toUpperCase(guessedLetter);
			if(isLetterInWord(word, toUpperCase(guessedLetter)) == -1){
				lives -= 1;
				if(lives == 1){
					System.out.println("\n" + guessedLetter + " is the wrong letter");
					System.out.println("You have " + lives + " life left...\n");
				}
				else{
				System.out.println("\n" + guessedLetter + " is the wrong letter");
				System.out.println("You have " + lives + " lives left...\n");
				}
			}
			else{
				letters[isLetterInWord(word, toUpperCase(guessedLetter))] =true;
				System.out.println("\n" + "LUCKY GUESS.\nYou have " + lives + " lives left\n");
			}
			printWork(word, letters);
			System.out.println("\n");
			if(letters[0] == false || letters[1] == false  || letters[2] == false  || letters[3] == false ){
				correct = false;
			}
			else{
				System.out.println("HOW'D YOU KNOW THE WORD WAS " + word + "?");
				correct = true;
				System.out.println("Congrats! You got it ");
			}
			if(lives==0){
				System.out.println("Oops! Better luck next time :)");
			}
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}