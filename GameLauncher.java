import java.util.Scanner;
public class GameLauncher
{
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		boolean gameNotContinued = false;
		//a While loop thats asks you if you want to play hangman or wordle,
		//Once you have plyed you will get the chance to replay.
		while(gameNotContinued==false){
			System.out.println("What game would you like to play?\n1) Hangman\n2) Wordle");
			int gameChoice = reader.nextInt();
			//if you chose 2 the wordle game runs and asks you if you want to play again.
			if(gameChoice == 2){
				String choiceAnswer = wordle.generateWord();
				wordle.runGame(choiceAnswer);
				System.out.println("\nWould you like to play another game?\n1) Yes\n 2) No\n");
				int playAgain = reader.nextInt();
				if(playAgain == 1){
					gameNotContinued = false;
				}
				if(playAgain == 2){
					gameNotContinued = true;
				}
			}
			//if you chose 2 the wordle game runs and asks you if you want to play again.
			if(gameChoice == 1){
				System.out.println("Enter a 4-letter word:");
				String word = reader.next();
				word = word.toUpperCase();
				System.out.print("\033c");
				Hangman.runGame(word);
				System.out.println("\nWould you like to play another game?\n1) Yes\n 2)No\n");
				int playAgain = reader.nextInt();
				if(playAgain == 1){
					gameNotContinued = false;
				}
				if(playAgain == 2){
					gameNotContinued = true;
				}
			}
			//If you have not chose a 1 or a 2 then you will be asked to choose a one or two again.
			else{
				System.out.println("\nPlease choose 1 or 2\n");
				gameNotContinued = false;
			}
		}
	}
}